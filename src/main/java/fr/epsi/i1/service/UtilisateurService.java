package fr.epsi.i1.service;

import fr.epsi.i1.UtilisateurDto;
import fr.epsi.i1.dao.UtilisateurDao;
import fr.epsi.i1.modele.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UtilisateurService {

    @Autowired
    private UtilisateurDao utilisateurDao = new UtilisateurDao();

    @Transactional
    public Utilisateur getUtilisateur(UtilisateurDto utilisateurDto){
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setLogin_user(utilisateurDto.getLogin());
        utilisateur.setPassword_user(utilisateurDto.getPassword());
        return utilisateurDao.getUtilisateur(utilisateur);
    }

}

