package fr.epsi.i1;


import javax.validation.constraints.NotNull;

public class UtilisateurDto {

    @NotNull(message = "Renseigner le login")
    private String login;

    @NotNull(message = "Renseigner le mot de passe")
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
