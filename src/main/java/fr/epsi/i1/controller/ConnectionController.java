package fr.epsi.i1.controller;

import fr.epsi.i1.UtilisateurDto;
import fr.epsi.i1.modele.Utilisateur;
import fr.epsi.i1.service.UtilisateurService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Scope("session")
public class ConnectionController {

    private UtilisateurService utilisateurService = new UtilisateurService();
    private Utilisateur utilisateur;

    @GetMapping({"/"})
    public String displayIndex(){
        return "index";
    }

    @PostMapping({"/"})
    public String connectUser(Model model, @Validated @ModelAttribute UtilisateurDto utilisateurDto, BindingResult bindingResult) {
        try {
            this.utilisateur = utilisateurService.getUtilisateur(utilisateurDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("utilisateur", this.utilisateur);
        return "index";
    }

}
