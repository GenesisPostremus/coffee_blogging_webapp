package fr.epsi.i1.modele;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "Utilisateur")
@Table(name = "Utilisateur")
public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user", unique = true, nullable = false)
    private Long id_user;

    @NonNull
    private String login_user;

    private String name_user;

    private String phone_number_user;

    private String email_adresse_user;

    @NonNull
    private String password_user;

    private boolean admin_ent;

    private String img_location;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_ent")
    private Entreprise entreprise;

    @OneToMany(mappedBy = "utilisateur", fetch = FetchType.EAGER)
    private Set<Post> posts;

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    @NonNull
    public String getLogin_user() {
        return login_user;
    }

    public void setLogin_user(@NonNull String login_user) {
        this.login_user = login_user;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getPhone_number_user() {
        return phone_number_user;
    }

    public void setPhone_number_user(String phone_number_user) {
        this.phone_number_user = phone_number_user;
    }

    public String getEmail_adresse_user() {
        return email_adresse_user;
    }

    public void setEmail_adresse_user(String email_adresse_user) {
        this.email_adresse_user = email_adresse_user;
    }

    @NonNull
    public String getPassword_user() {
        return password_user;
    }

    public void setPassword_user(@NonNull String password_user) {
        this.password_user = password_user;
    }

    public boolean isAdmin_ent() {
        return admin_ent;
    }

    public void setAdmin_ent(boolean admin_ent) {
        this.admin_ent = admin_ent;
    }

    public String getImg_location() {
        return img_location;
    }

    public void setImg_location(String img_location) {
        this.img_location = img_location;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
}
