package fr.epsi.i1.modele;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "Entreprise")
@Table(name = "Entreprise")
public class Entreprise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ent", unique = true, nullable = false)
    private Long id_ent;

    private String nom_ent;

    private String num_tel_ent;

    private String num_adresse_ent;

    private String rue_adresse_ent;

    private String cp_adresse_ent;

    private String ville_adresse_ent;

    private String logo_location;

    @OneToMany(mappedBy = "entreprise", fetch = FetchType.EAGER)
    private Set<Utilisateur> utilisateurs;

    public Set<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(Set<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    public Long getId_ent() {
        return id_ent;
    }

    public void setId_ent(Long id_ent) {
        this.id_ent = id_ent;
    }

    public String getNom_ent() {
        return nom_ent;
    }

    public void setNom_ent(String nom_ent) {
        this.nom_ent = nom_ent;
    }

    public String getNum_tel_ent() {
        return num_tel_ent;
    }

    public void setNum_tel_ent(String num_tel_ent) {
        this.num_tel_ent = num_tel_ent;
    }

    public String getNum_adresse_ent() {
        return num_adresse_ent;
    }

    public void setNum_adresse_ent(String num_adresse_ent) {
        this.num_adresse_ent = num_adresse_ent;
    }

    public String getRue_adresse_ent() {
        return rue_adresse_ent;
    }

    public void setRue_adresse_ent(String rue_adresse_ent) {
        this.rue_adresse_ent = rue_adresse_ent;
    }

    public String getCp_adresse_ent() {
        return cp_adresse_ent;
    }

    public void setCp_adresse_ent(String cp_adresse_ent) {
        this.cp_adresse_ent = cp_adresse_ent;
    }

    public String getVille_adresse_ent() {
        return ville_adresse_ent;
    }

    public void setVille_adresse_ent(String ville_adresse_ent) {
        this.ville_adresse_ent = ville_adresse_ent;
    }

    public String getLogo_location() {
        return logo_location;
    }

    public void setLogo_location(String logo_location) {
        this.logo_location = logo_location;
    }
}
