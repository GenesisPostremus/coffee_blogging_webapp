package fr.epsi.i1.modele;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Post")
@Table(name = "Post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_post", unique = true, nullable = false)
    private Long id_post;

    @NonNull
    private String content_post;

    @NonNull
    private Date date_post;

    private Long likes_post;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user")
    private Utilisateur utilisateur;

    public Long getId_post() {
        return id_post;
    }

    public void setId_post(Long id_post) {
        this.id_post = id_post;
    }

    @NonNull
    public String getContent_post() {
        return content_post;
    }

    public void setContent_post(@NonNull String content_post) {
        this.content_post = content_post;
    }

    @NonNull
    public Date getDate_post() {
        return date_post;
    }

    public void setDate_post(@NonNull Date date_post) {
        this.date_post = date_post;
    }

    public Long getLikes_post() {
        return likes_post;
    }

    public void setLikes_post(Long likes_post) {
        this.likes_post = likes_post;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
