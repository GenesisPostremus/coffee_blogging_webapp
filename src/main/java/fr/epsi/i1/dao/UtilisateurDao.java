package fr.epsi.i1.dao;

import fr.epsi.i1.modele.Utilisateur;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Repository
public class UtilisateurDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Utilisateur getUtilisateur(Utilisateur utilisateur){
        return entityManager.createQuery("select u from Utilisateur u where u.login_user = :login or u.email_adresse_user = :email and u.password_user = :pass", Utilisateur.class)
                .setParameter("login", utilisateur.getLogin_user())
                .setParameter("email", utilisateur.getEmail_adresse_user())
                .setParameter("pass", utilisateur.getPassword_user())
                .getSingleResult();
    }

}
