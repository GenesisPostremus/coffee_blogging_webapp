document.getElementById('addPost').addEventListener("click", addPost);
document.getElementById('closePost').addEventListener("click", closePost);


function addPost() {
    document.getElementById('postContainer').classList.remove("nopresent");
    document.getElementById('postContainer').classList.add("present");
}

function closePost() {
    document.getElementById('postContainer').classList.remove("present");
    document.getElementById('postContainer').classList.add("nopresent");
}