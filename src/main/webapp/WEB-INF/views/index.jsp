<%@page pageEncoding="UTF-8" contentType="text/html"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<c:url value="/resources/css/reset.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">
    <title>Document</title>
</head>
<body>
<div class="leftPart">
    <div class="leftContainer">
        <section class="secLogo">
            <img src="<c:url value="/resources/img/logo.png"/>" class="logo" alt="logo">
            <h1>Coffee</h1>
        </section>
        <nav class="menu">
            <a class="active" href=""> <img src="<c:url value="/resources/img/home.png"/>" class="icon" alt=""> Accueil</a>
            <a href=""> <img src="<c:url value="/resources/img/profil.png"/>" class="icon" alt=""> Mon profil</a>
            <a href=""> <img src="<c:url value="/resources/img/signet.png"/>" class="icon" alt=""> Informations</a>
        </nav>
        <section class="secCompanies">
            <img src="<c:url value="/resources/img/companies.png"/>" class="logoCompanies" alt="logo">
            <div>
                <h2>Entreprise</h2>
                <strong>Unknown Companies</strong>
            </div>
        </section>
    </div>
</div>
<main class="centerPart">
    <div class="searchContainer">
        <input class="search" type="text" placeholder="Rechercher">
    </div>
    <section class="post">
        <div class="postSide">
            <img class="profilPost" src="<c:url value="/resources/img/profil.png"/>" alt="">
        </div>
        <div class="postMain">
            <h3>User unknown</h3>
            <span class="datePost">7 Spt</span>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s.
            </p>
            <nav class="postNav">
                <div>
                    <img class="like" src="<c:url value="/resources/img/like.png"/>" alt="">
                    <img class="comment" src="<c:url value="/resources/img/comment.png"/>" alt="">
                </div>
                <img class="more" src="<c:url value="/resources/img/more.png"/>" alt="">
            </nav>
        </div>
    </section>
</main>
<div class="rightPart">
    <div class="rightContainer">
        <div class="editPack">
            <img class="profilPic" src="<c:url value="/resources/img/profilPic.png"/>" alt="">
            <h2>User unknown</h2>
            <input class="inputPoster" type="submit" value="Poster" id="addPost">
        </div>
        <input class="inputPoster" type="submit" value="Se déconnecter">
    </div>
</div>
<section id="postContainer" class="backgroundPost nopresent">
    <div class="formPost">
        <header>
            <h3>Ajouter un Post</h3>
            <a id="closePost" href=""><img class="cross" src="<c:url value="/resources/img/cross.png"/>" alt=""></a>
        </header>
        <section class="mainForm">
            <img src="<c:url value="/resources/img/profil.png"/>" alt="">
            <textarea class="textarea" name="" id="" placeholder="Votre message"></textarea>
        </section>
        <footer>
            <img class="addFi" src="<c:url value="/resources/img/addFi.png"/>" alt="">
            <input class="inputPoster" type="submit" value="Poster">
        </footer>
    </div>
</section>
</body>

<script src="<c:url value="/resources/js/main.js"/>"></script>
</html>
